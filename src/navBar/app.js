import Vue from 'vue';
import singleSpaVue from 'single-spa-vue';
import NavBar from './main.vue'

const vueLifecycles = singleSpaVue({
    Vue,
    appOptions: {
        el: '#navBar',
        render: r => r(NavBar)
    }
});

export const bootstrap = [
    vueLifecycles.bootstrap,
];

export const mount = [
    vueLifecycles.mount,
];

export const unmount = [
    vueLifecycles.unmount,
];
