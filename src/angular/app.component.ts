import {Component} from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
    selector: 'angular-root',
    templateUrl: 'src/angular/app.component.html'
})
export class AppComponent {
    todoArray: Array<string> = [];

    addTodo(form: NgForm) {
        if (form.valid) {
            this.todoArray.push(form.value.todo);
            form.reset();
        }
    }

    deleteItem(todo: string) {
        this.todoArray = [...this.todoArray.filter((el) => el !== todo)];
    }
}




