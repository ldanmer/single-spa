import * as singleSpa from 'single-spa';

singleSpa.registerApplication('navBar', () =>  import ('../navBar/app.js'), () => true);
singleSpa.registerApplication('angular', () =>  import ('../angular/app.js'), pathPrefix('/angular'));
singleSpa.registerApplication('ngJS', () => import('../ngJS/app.js'), pathPrefix('/ngJS'));
singleSpa.registerApplication('react', () =>  import ('../react/app.js'), pathPrefix('/react'));


singleSpa.start();

function pathPrefix(prefix) {
    return function (location) {
        return location.pathname.startsWith(`${prefix}`);
    }
}
