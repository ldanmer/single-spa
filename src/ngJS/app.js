import singleSpaAngularJS from 'single-spa-angularjs';
import angular from 'angular';
import './app.module.js';
import './routes.js';

const angularLifecycles = singleSpaAngularJS({
  angular,
  domElementGetter,
  mainAngularModule: 'angularJS-app',
  uiRouter: true,
  preserveGlobal: false,
});

export const bootstrap = [
  angularLifecycles.bootstrap,
];

export const mount = [
  angularLifecycles.mount,
];

export const unmount = [
  angularLifecycles.unmount,
];

function domElementGetter() {
  // Make sure there is a div for us to render into
  let el = document.getElementById('ngJS');
  if (!el) {
    el = document.createElement('div');
    el.id = 'ngJS';
    document.body.appendChild(el);
  }

  return el;
}
